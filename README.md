# Scrapper du site au-magasin.fr

## Installation
L'installation se fait via virtualenv.

## Utilisation
Scrappe le site pour récupérer la liste des magasins Micromania.  
Fait en executant le script scrapper.py.  
La liste des villes interrogées peut être modifiée dans le script suivant la localisation.  
Retourne un .json contenant la liste des magasins ainsi qu'un csv.  

``` javascript
{
    "address": "Centre commercial BEAUGRENELLE Rue de Linois 75015 paris 15e",
    "lat": 48.84833687,
    "id": "6662",
    "lng": 2.28397166,
    "name": "Micromania 6662"
}
```