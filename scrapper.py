import urllib.request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import geocoder
import csv
import json

def scrap():
    """
        Fonction de scrapping du site au-magasin.fr
        Pour recuperer la liste des magasins micromania.
    """

    villes=('paris', 'marseille', 'bordeaux', 'toulouse')

    base_url = "http://www.au-magasin.fr/recherche/results/?quoi=micromania&ou="

    # Iterate while next page exists
    list_magasins = []
    num_mag = 0
    for ville in villes:
        url = base_url + ville
        req = urllib.request.Request(url=url)
        print(url)

        # tries to read the page, stops if error
        try:
            handler = urllib.request.urlopen(req, timeout=5)
            page = handler.read()
        except HTTPError:
            print("Erreur dans la request HTTP")
            raise HTTPError

        ex_dict_magasin = {'nom':'',
                            'id':'',
                            'address':'',
                            'lat':'0',
                            'lng':'0'}

        soup = BeautifulSoup(page, 'html.parser')
        magasins = soup.body.find_all("h3", {"class":"item-h3"})
        addresses = soup.body.find_all("script", {"type":'text/javascript'})
        for address in addresses:
            if address.getText():
                elements = address.getText().split('[')
                for elem in elements:
                    if 'Micromania' in elem:
                        dict_magasin = {}
                        infos_magasin = elem.split(',')
                        dict_magasin['lat'] = float(infos_magasin[1].strip(' '))
                        dict_magasin['lng'] = float(infos_magasin[2].strip(' '))
                        dict_magasin['id'] = str(infos_magasin[3].strip(' '))
                        dict_magasin['name'] = "Micromania " + str(infos_magasin[3].strip(' '))
                        address = infos_magasin[5].strip('"')
                        if 'http' not in infos_magasin[6]:
                            address +=  ' ' + infos_magasin[6].strip('"')
                        for info in infos_magasin:
                            if 'http' in info:
                                address += ' ' + ' '.join(info.split('/')[3].split('-'))
                        
                        pattern = re.compile("<br />")
                        # if re.search(pattern, address):
                        #     address_cleaned = re.search(pattern, address).group()
                        # else:
                        address_cleaned = re.sub('<br />', ' ',address)
                        dict_magasin['address'] = address_cleaned
                        list_magasins.append(dict_magasin)
                        print(dict_magasin)

    with open('dict.csv', 'w') as csv_file:
        w = csv.DictWriter(csv_file, list_magasins[0].keys())
        w.writeheader()
        for magasin in list_magasins:
            w.writerow(magasin)

    with open('out.json', 'w') as fp:
        json.dump(list_magasins, fp, indent=4)

if __name__ == '__main__':
    scrap()
